﻿using System;
using System.Text;

namespace FirstDotnetConsoleProject
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Ders1
            ///// Not: Ctrl+K + Ctrl+D Seçili Kodu düzenler.
            ///// Not2: Ctrl+K + Ctlr+C Seçili kodu yorum satırı yapar

            //Console.WriteLine("hello");
            //Console.WriteLine("ozgur");
            //Console.WriteLine("dogancay");
            ////yorum
            //char comma;
            //comma = '0';
            //Console.WriteLine(comma);
            //var dnm = comma; // Var eşitlendiği objenin değişken tipini alır yada convert edilen tipi alır içinde ediyorsan.
            //Console.WriteLine(dnm);

            //var name = "Özgür";
            ///// String bir char dizisi olduğu için for döngüsü kullanabiliriz.
            //for (int i = 0; i < name.Length - 1; i++)
            //{
            //    Console.WriteLine(name[i]);
            //    Console.WriteLine((int)name[i]);
            //}

            ///// String bir char dizisi olduğu için foreach döngüsü kullanabiliriz.
            //foreach (var item in name)
            //{
            //    var dnm2 = Convert.ToInt64(item);
            //    Console.WriteLine(item);
            //    Console.WriteLine((int)item);// Chartı'int yönetemi ile hex koduna çevirebil
            //    Console.WriteLine(Convert.ToInt64(item));// burda bir diğer convert kullanımı
            //}
            #endregion

            #region Ders2


            //int[] studentId = new int[3] { 3, 6, 7 }; // 3 Elemanlı bir array oluşturduk
            //foreach (var item in studentId)
            //{
            //    Console.WriteLine(item);

            //}
            //var cikti2 = "{0}. Öğrenci: {1}";

            //string[] studentNames = new string[5] { "Özgür", "Hüseyin", "Mert", "Asker", "Melih" };
            //for (int i = 0; i < studentNames.Length; i++)
            //{
            //    //Console.WriteLine(i+1+". Öğrenci "+studentNames[i]);// for döngüsünde indis numarasını yazmamız gerek
            //    string cikti = $"{i + 1} .öğrenci {studentNames[i]}";
            //    Console.WriteLine(cikti);

            //    Console.WriteLine(string.Format(cikti2, i + 1, studentNames[i])); // yukardaki değerleri indisten çekiyor




            //}
            //int sayac = 1;

            //foreach (var item in studentNames)
            //{

            //    //Console.WriteLine(sayac + ". öğrenci " + item);
            //    Console.WriteLine($"{sayac} .Öğrenci {item}");// Bir diğer string birleştirme işlemi
            //    sayac++;

            //}

            #endregion

            #region Zamanlar


            //DateTime currentDate = DateTime.Now; // Basit datetime fonksiyonu oluşturma

            //Console.WriteLine(currentDate);
            //Console.WriteLine("DateTime: {0},{1}", currentDate, "naber");
            //Console.WriteLine("DateTime: {0}", currentDate);
            //Console.WriteLine("Only Date: {0:D}", currentDate);
            //Console.WriteLine("Only Time: {0:T}", currentDate);
            //Console.WriteLine("Only Year: {0:Y}", currentDate);
            //Console.WriteLine(currentDate.ToString("d MMMM dddd yyyy"));
            #endregion

            #region StringFonksiyonları

            string output = ",Mert altun geldi gitii : altuncum altun";
            // string newOutput = output.Trim(); Direkt olarak baştaki sondaki boşluk siler
            string newOutput = output.Trim(',');  // Char ile neyin silineceğini sen belirle baş ve son
                                                  //string newOutput2 = output.Replace("altun", string.Empty); Boşaltıyor içini stringin
            string newOutput2 = output.Replace("altun", "doğançay");// Belirli bir şeyi başka birşey yapma


            string newOutput3 = output.Replace(":", string.Empty);

            string newOutput4 = output.Remove(0, 1); // Belirli aralıktakileri siler

            string newOutput5 = output.Substring(7, 11); // ilk girdiğim başlangıç son girdiğin o kadar devam et


            Console.WriteLine("Eski hali: " + output);
            Console.WriteLine("Yeni hali: " + newOutput);
            Console.WriteLine("Yeni hali: " + newOutput2);

            Console.WriteLine("Yeni hali: " + newOutput3);

            Console.WriteLine($"Bu metin {output.Length} karakterdir. "); // + Birleştirme yerine kullandım.


            Console.WriteLine("Yeni hali: " + newOutput4);
            Console.WriteLine("Yeni hali: " + newOutput5);

            string name1 = "Gökhan";
            string name2 = "Özgür";
            string name3 = "Gökhan";

            Console.WriteLine(string.Compare(name1, name2));// Karşılaştırma yapar 0 derse aynı -1 derse değil
            Console.WriteLine(name1 == name2 ? "eşit":"Eşit Değil");// Gerçek hayat kullanımı short if

            // && Ve demek || veya demek == eşitse demek != eşit değilse.
            if (name1 != name3 || name1=="Özgür")
            {
                Console.WriteLine("Aynı deği");
            }
            else
            {
                Console.WriteLine("Aynı ");
            }

            var sira = output.IndexOf("altuncum"); // kaçıncı sıradan başladığını belirtir // substring ile kullanabiliriz

            Console.WriteLine(sira);
            /// lastindexof geriden sayar
            /// 
            bool isStart = output.StartsWith(",Mert"); // String mert ilemi başlıyor
            bool isEnd = output.EndsWith("altun"); // String altun ilemi bitiyor
       
            Console.WriteLine(output.ToLower()); // belirli  bir stringin tüm harflerini küçültür

            Console.WriteLine(output.ToUpper());  // belirli  bir stringin tüm harflerini büyük yapar


            string value = "Özgür amcam starbuc kstan coffee içer";

            var indexNo = value.IndexOf("coffee");

            var newValue = value.Insert(indexNo, "chocolate "); // parametrede verilen numaranın yanına string ekler

            string[] dizi = value.Split(" "); // Değerlerimizi bölüyoruyz her boşluktan sonra böl 

            foreach (var item in dizi)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine(value);
            Console.WriteLine(newValue);
            string newValue2 = string.Join("||", dizi); // bölünmüşe parametreyle birleştirir
            Console.WriteLine(newValue2);
           





            #endregion
        }
    }
}
